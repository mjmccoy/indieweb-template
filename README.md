# indieweb-template

This repository provides minimal HTML for starting a new [IndieWeb](https://indieweb.org) site!

## How to play

1. Fork this repository to your user account.
2. Rename it to `<yourusername>.gitlab.io`, this can be done by navigating to your
project's **Settings**.
3. Edit `index.html` and put stuff there.
4. (Optional) [Point your domain to GitLab](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/).

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

---

This project is forked from [blank-gh-site](https://github.com/indieweb/blank-gh-site).

Per [CC0](http://creativecommons.org/publicdomain/zero/1.0/), to the extent possible under law, [the contributors](https://github.com/indieweb/blank-gh-site/graphs/contributors) have waived all copyright and related or neighboring rights to this work.
